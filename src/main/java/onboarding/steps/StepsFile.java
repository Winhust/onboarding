package onboarding.steps;

import onboarding.pages.PagesFile;

public class StepsFile {
    PagesFile signInPage;

    public void signIn(String email, String password) {
        signInPage.enterEmail(email);
        signInPage.enterPassword(password);
    }

    public void clickBtnSignIn() {
        signInPage.clickBtn("Sign in");
    }

    public void verifymessage(String msg) {
        signInPage.verifyMessage(msg);
    }


}

