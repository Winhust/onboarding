Feature: onboarding


  Scenario: Sign in Shopbase
    Given open form login shopbase
    When Enter email and password
      | Email                      | Password | Message                              |
      |                            | 123456   | Please enter your email and password |
      | thangnguyen2@beeketing.net |          | Please enter your email and password |
      | thangnguyen2               | 123456   | Email or password is not valid       |
      | thangnguyen2@beeketing.net | 123456   | Success                              |
