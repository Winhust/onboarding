package onboarding.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PagesFile {
    WebDriver driver;

    public WebElement getElement(String xpath){
        return  driver.findElement(By.xpath(xpath));
    }

    public void waitElementVisible(String xpath) {
        WebDriverWait driverWait = new WebDriverWait(driver, 10);
        driverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
    }

    public void inputToElement(String xpath, String value) {
        waitElementVisible(xpath);
        WebElement e = getElement(xpath);
        e.clear();
        e.sendKeys(value);
    }

    public void clickElement(String xpath) {
        waitElementVisible(xpath);
        getElement(xpath).click();
    }

    public void enterEmail(String email) {
        inputToElement("//input[@id='email']", email);
    }

    public void enterPassword(String password) {
        inputToElement("//input[@id='password']", password);
    }

    public void clickBtn(String value) {
        String xpath = "//button[child::span[normalize-space()='"+value+"']]";
        clickElement(xpath);
    }
    public  void verifyMessage(String msg){
        String ms1 = getText("//div[@class='form-text next-span next-span__reset-pw']//p");
        ms1.equalsIgnoreCase(msg);
    }


    public String getText(String xpath) {
        return driver.findElement(By.xpath(xpath)).getText();
    }


}

