package Def;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import onboarding.steps.StepsFile;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.seleniumhq.jetty9.server.session.SessionData;

import java.util.List;

public class onboardingDef {
    WebDriver driver;

    @Steps
    StepsFile signInSteps;
    @When("Enter email and password")
    public void enterEmailAndPassword (List<List<String>> dataTable) {
        for (int row : SessionData.getDataTbRowsNoHeader(dataTable).keySet()) {
            String email = SessionData.getDataTbVal(dataTable, row, "Email");
            String password = SessionData.getDataTbVal(dataTable, row, "Password");
            String msg = SessionData.getDataTbVal(dataTable, row, "Message");
            signInSteps.signIn(email, password);
            signInSteps.clickBtnSignIn();
            if (msg.equals("Success")) {
                System.out.println("Success login");
            } else {
                signInSteps.verifymessage(msg);
            }
        }
    }


    @Given("open form login shopbase")
    public void openUrl() {
        String web = "https://accounts.shopbase.com/sign-in";
        System.setProperty("webdriver.chrome.driver", "src/test/resources/webdriver/Chrome/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(web);
        driver.manage().window().maximize();


    }
